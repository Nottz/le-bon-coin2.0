import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AdComponent } from './ad/ad.component';
import {FormsModule} from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AdService } from './services/ad.service';



@NgModule({
  declarations: [
    AppComponent,
    AdComponent
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    FormsModule
  ],
  providers: [
    AdService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
