import { Injectable } from '@angular/core';

@Injectable()

export class AdService {

  ads = [
    {
      name: 'Moto à 6 roues',
      status: 'Publiée',
      date: new Date(),
      price: 1.333333
    },
    {
      name: 'Chien à 3 têtes',
      status: 'Brouillon',
      date: new Date(),
      price: 200
    },
    {
      name: 'Valse à 1000 temps',
      status: 'Désactivée',
      date: new Date(),
      price: 10.5
    },
  ];

  constructor() { }

  publishAll() {
    this.ads.forEach(ad => ad.status = 'Publiée');
  }

  publishOne(i) {
    this.ads[i].status = 'Publiée';
  }

  draftOne(i) {
    this.ads[i].status = 'Brouillon';
  }

  desactivateOne(i) {
    this.ads[i].status = 'Désactivée';
  }

}
