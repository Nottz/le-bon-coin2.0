import { Component, Input, OnInit } from '@angular/core';
import { AdService } from '../services/ad.service';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {

  @Input() adName: string;
  @Input() adStatus: string;
  @Input() adDate: Date;
  @Input() adPrice: number;
  @Input() index: number;


  lastUpdate = new Date();

  constructor(private adService: AdService) { }

  ngOnInit() {
  }

  getStatus() {
    return this.adStatus;
  }

  onSwitch() {
    if (this.adStatus === 'Désactivée' || 'Brouillon') {
      this.adService.publishOne(this.index);
    } else if (this.adStatus === 'Publiée' || 'Brouillon') {
      this.adService.desactivateOne(this.index);
    } else if (this.adStatus === 'Publiée' || 'Désactivée') {
      this.adService.draftOne(this.index);
    }
  }


  getColor() {
    if (this.adStatus === 'Publiée') {
      return 'green';
    } else if (this.adStatus === 'Désactivée') {
      return 'red';
    } else {
      return 'grey';
    }
  }

}
