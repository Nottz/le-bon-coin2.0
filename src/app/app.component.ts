import { Component, OnInit } from '@angular/core';
import { AdService } from './services/ad.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  isAuthenticated = false;

  ads: any[];

  constructor(private adService: AdService) {



  }

  onPublier() {
    this.adService.publishAll();
  }


  ngOnInit() {

    this.ads = this.adService.ads;

  }

  login() {
    return this.isAuthenticated = false;
  }

  logout() {
    return this.isAuthenticated = true;
  }

}
